#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include"library.h"
#include"date.h"
/*void date_tester() {
	date_t d1 = {1, 1, 2000}, d2 = {31, 12, 2000};
	date_t d = {1, 1, 2000};
	date_t r = date_add(d, 366);
	date_print(&r);
	int diff = date_cmp(d1, d2);
	printf("date diff: %d\n", diff);
}*/

/*void tester() {
	user_t u;
	book_t b;
	user_accept(&u);
	user_display(&u);
	book_accept(&b);
	book_display(&b);
}*/
void sign_in()
{
    char email[30],password[30];
    user_t u;
	int invalid_user = 0;
    printf("Email: ");
    scanf("%s",email);
    
    if(search_user_by_email(&u,email)==1)
    {
        printf("email verified.. \n");
		printf("Password: ");
    	scanf("%s",password);
		if(strcmp(u.password,password)==0)
        {
            printf("password verified.. \n");
			if(strcmp(email,EMAIL_OWNER)==0)
            	strcpy(u.role,ROLE_OWNER);
			if(strcmp(u.role,ROLE_OWNER)==0)
                owner_area(&u);
			else if(strcmp(u.role,ROLE_LIBRARIAN)==0)
				librarian_area(&u);
			else if(strcmp(u.role,ROLE_MEMBER)==0)
				member_area(&u);
			else
				invalid_user = 1;
        }
		else
			printf("Password not matched\n");
    }
	else
		printf("Email not found...\n");
}

void sign_up()
{
    user_t u;
    user_accept(&u);
    user_add(&u);
}

int main()
{
    //tester();
    int choice;
	do {
		printf("\n\n0. Exit\n1. Sign In\n2. Sign Up\nEnter choice: ");
		scanf("%d", &choice);
		switch (choice)
		{
		case 1: // Sign In
			sign_in();
			break;
		case 2:	// Sign Up
			sign_up();	
			break;
		}
	}while(choice != 0);
    return 0;
}