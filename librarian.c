#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include"library.h"
#include"date.h"

void librarian_area(user_t *u)
{
   char name[80];
    int choice,memberid;
    while((choice=lab_menu_list())!=0)
    {
        
        switch(choice)
        {
            
            case 1: 
                add_member();
            break;
            case 2:
                edit_profile(u);
             break;
            case 3: 
                edit_password(u);
            break;
            case 4: 
                 add_book();
            break;
            case 5: 
                printf("Enter book name: ");
                scanf("%s", name);
                book_find_by_name(name);
            break;
            case 6: 
                book_edit_by_id();
            break;
            case 7: 
                 bookcopy_checkavail_details();
            break;
            case 8: 
                 bookcopy_add();
            break;
            case 9: 
                change_rack();
            break;
            case 10: 
                bookcopy_issue();
            break;
            case 11: 
                 bookcopy_return();
            break;
            case 12: 
                 fees_payment_add();
            break;
            case 13:
            printf("enter member id of the member: ");
			scanf("%d", &memberid);
			payment_history(memberid);
             break;
        }
    }
}

int lab_menu_list()
{
    int choice;
    printf("0. sign out\n");
    printf("1. Add Member\n");
    printf("2. Edit profile\n");
    printf("3. change password\n");
    printf("4. Add book\n");
    printf("5. find book\n");
    printf("6. edit book\n");
    printf("7. check availibility\n");
    printf("8. Add copy\n");
    printf("9. change rack\n");
    printf("10. issue copy\n");
    printf("11. return copy\n");
    printf("12. Take payment\n");
    printf("13. payment history\n");
    printf("Enter choice ");
    scanf("%d",&choice);
    return choice;
}

void add_member()
{
    user_t u;
    user_accept(&u);
    user_add(&u);
}
void add_book()
{
    printf("add book..\n");
    book_t b;
    book_accept(&b);
    FILE *fp= fopen(BOOK_DB,"ab");
    fwrite(&b,sizeof(book_t),1,fp);
    fclose(fp);
    
}
void book_edit_by_id()
{
    int found=0;
    size_t size= sizeof(book_t);
    book_t b;
    int id;
    FILE *fp1=fopen(BOOK_DB,"rb");
	if(fp1==NULL)
    {
        perror("File is not found\n");
        return;
    }
	while(fread(&b,sizeof(book_t),1,fp1)>0)
    {
    	 book_display(&b);
    }
	fclose(fp1);
    printf("Enter ID of book ");
    scanf("%d",&id);
    FILE *fp=fopen(BOOK_DB,"rb+");
    if(fp==NULL)
    {
        perror("File c\n");
        return;
    }
    while(fread(&b,size,1,fp)>0)
    {
        if(b.id==id)
        {
            found=1;
            break;
        }
    }
    if(found)
    {
        book_t nb;
        book_accept(&nb);
        nb.id=id;
        fseek(fp,-size,SEEK_CUR);
        fwrite(&nb,size,1,fp);
      
    }
    else
    {
        printf("no such book found..\n");
    }
    fclose(fp);
}

void bookcopy_add()
{
    bookcopy_t b;
    book_t b1;
    FILE *fp1= fopen(BOOK_DB,"rb");
    if(fp1==NULL)
    {
        printf("No book is added till now...\n");

    }
    while(fread(&b1,sizeof(book_t),1,fp1)>0)
    {
        book_display(&b1);
    }
    fclose(fp1);
    bookcopy_accept(&b);
    b.id=get_next_bookcopy_id();
    FILE *fp= fopen(BOOKCOPY_DB,"ab");
    if(fp==NULL)
    {
        perror("Cant open file\n");
        return;
    }
    fwrite(&b,sizeof(bookcopy_t),1,fp);
    printf("Copy added successfully...\n");
    fclose(fp);


}

void bookcopy_checkavail_details()
{
    int book_id,ct=0;
    bookcopy_t bc;
    book_t b;
    FILE *fp1=fopen(BOOK_DB,"rb");
	if(fp1==NULL)
    {
        perror("File is not found\n");
        return;
    }
	while(fread(&b,sizeof(book_t),1,fp1)>0)
    {
    	 book_display(&b);
    }
	fclose(fp1);
    printf("enter the book id: ");
	scanf("%d", &book_id);
    FILE *fp= fopen(BOOKCOPY_DB,"rb");
    if(fp==NULL)
    {
        perror("File is not found\n");
        return;
    }
    while(fread(&bc,sizeof(bookcopy_t),1,fp)>0)
    {
        if((bc.bookid==book_id)&&strcmp(bc.status,STATUS_AVAIL)==0)
        {
            bookcopy_display(&bc);
            ct++;
        }
    }
    fclose(fp);
    if(ct==0)
    printf("No such book is available\n");
}

void bookcopy_issue() {
	issuerecord_t rec;
	FILE *fp;
	issuerecord_accept(&rec);
	rec.id = get_next_issuerecord_id();
	if(!is_paid_member(rec.memberid)) 
    {
		printf("member is not paid.\n");
		return;
	}
	fp = fopen(ISSUERECORD_DB, "ab");
	if(fp == NULL) {
		perror("issuerecord file cannot be opened");
		return;
	}
	fwrite(&rec, sizeof(issuerecord_t), 1, fp);
	fclose(fp);
	bookcopy_changestatus(rec.copyid, STATUS_ISSUED);
}
void bookcopy_changestatus(int bookcopy_id, char status[])
{
    bookcopy_t bc;
    FILE *fp= fopen(BOOKCOPY_DB,"rb+");
    if(fp==NULL)
    {
        perror("file doesnt exist\n");
        return;
    }
    while(fread(&bc,sizeof(bookcopy_t),1,fp)>0)
    {
        if((bc.id==bookcopy_id))
        {
            strcpy(bc.status,status);
            fseek(fp, -sizeof(bookcopy_t), SEEK_CUR);
            fwrite(&bc, sizeof(bookcopy_t), 1, fp);
            break;

        }
    }
    fclose(fp);
}
void bookcopy_return() 
{
	int member_id, record_id;
	FILE *fp;
	issuerecord_t rec;
	int diff_days, found = 0;
	long size = sizeof(issuerecord_t);
	printf("enter member id: ");
	scanf("%d", &member_id);
	display_issued_bookcopies(member_id);
	printf("enter issue record id (to return): ");
	scanf("%d", &record_id);
	fp = fopen(ISSUERECORD_DB, "rb+");
	if(fp==NULL) 
	{
		perror("cannot open issue record file");
		return;
	}
	while(fread(&rec, sizeof(issuerecord_t), 1, fp) > 0) 
	{
		if(record_id == rec.id) 
		{
			found = 1;
			rec.return_date = date_current();
			diff_days = date_cmp(rec.return_date, rec.return_duedate);
			if(diff_days > 0)
				{
                    rec.fine_amount = diff_days * FINE_PER_DAY;
			        fine_payment_add(rec.memberid, rec.fine_amount);
				    printf("fine amount Rs. %.2lf/- is applied.\n", rec.fine_amount);
                }
                break;
		}
	}
	
	if(found) 
	{
		fseek(fp, -size, SEEK_CUR);
		fwrite(&rec, sizeof(issuerecord_t), 1, fp);
		printf("issue record updated after returning book:\n");
		issuerecord_display(&rec);
		bookcopy_changestatus(rec.copyid, STATUS_AVAIL);
	}
	fclose(fp);
}
void display_issued_bookcopies(int member_id) {
	FILE *fp;
	issuerecord_t rec;
	fp = fopen(ISSUERECORD_DB, "rb");
	if(fp==NULL) {
		perror("cannot open issue record file");
		return;
    }
	while(fread(&rec, sizeof(issuerecord_t), 1, fp) > 0) {
		if(rec.memberid == member_id && rec.return_date.day == 0)
			issuerecord_display(&rec);
	}
	fclose(fp);
}

void change_rack()
{
    bookcopy_t bc;
    int boockcopy_id,nrack,found=0;
    long size= sizeof(bookcopy_t);
    FILE *fp= fopen(BOOKCOPY_DB,"rb+");
    if(fp==NULL)
    {
        perror("file can't be opened\n");
        return;
    }
    while(fread(&bc,size,1,fp)>0)
    {
        if(strcmp(bc.status,STATUS_AVAIL)==0)
        bookcopy_display(&bc);
    }
    fseek(fp,0,SEEK_SET);
    printf("Enter boocopy id: ");
    scanf("%d",&boockcopy_id);
    printf("Enter new rack position: ");
    scanf("%d",&nrack);
    while(fread(&bc,size,1,fp)>0)
    {
        if((bc.id==boockcopy_id)&&(strcmp(bc.status,STATUS_AVAIL)==0))
        {
            found=1;
            bc.rack=nrack;
            break;
        }
    }
    if(found)
    {
    fseek(fp,-size,SEEK_CUR);
    fwrite(&bc,size,1,fp);
    printf("Rack is updated successfully..\n");
    }
    else
    {
        printf("No such book is available\n");
    }
    
    fclose(fp);
    bookcopy_display(&bc);

}

void fees_payment_add()
{
    payment_t p;
     payment_accept(&p);
    p.id=get_next_paymentrecord_id();
    FILE *fp=fopen(PAYMENT_DB,"ab");
    if(fp==NULL)
    {
        perror("File can't be opened \n");
        return;
    }
    fwrite(&p,sizeof(payment_t),1,fp);
    fclose(fp);
    printf("Record added successfully...\n");
   
}
int is_paid_member(int memberid)
{
    date_t now= date_current();
    payment_t p;
    int paid=0;
    long size=sizeof(payment_t);
    FILE *fp=fopen(PAYMENT_DB,"rb");
    if(fp==NULL)
    {
        perror("File can't be opened \n");
        return 0;
    }
    while(fread(&p,size,1,fp)>0)
    {
        if(p.userid==memberid&&p.nextpayment_duedate.day != 0&&date_cmp(now,p.nextpayment_duedate)<0)
        {
            paid=1;
            break;
        }
    }
    fclose(fp);
    return paid;


}
void fine_payment_add(int memberid, float fine_amount) {
	FILE *fp;
	// initialize fine payment
	payment_t pay;
	pay.id = get_next_paymentrecord_id();
	pay.userid = memberid;
	pay.amount = fine_amount;
	strcpy(pay.type, PAY_TYPE_FINE);
	pay.tx_time = date_current();
	memset(&pay.nextpayment_duedate, 0, sizeof(date_t));
	// open the file
	fp = fopen(PAYMENT_DB, "ab");
	if(fp == NULL) {
		perror("cannot open payment file");
		exit(1);
	}
	// append payment data at the end of file
	fwrite(&pay, sizeof(payment_t), 1, fp);
	// close the file
	fclose(fp);
}

void payment_history(int memberid) {
	FILE *fp;
	payment_t pay;
	// open file
	fp = fopen(PAYMENT_DB, "rb");
	if(fp==NULL) {
		perror("cannot open payment file");
		return;
	}
	// read payment one by one till eof
	while(fread(&pay, sizeof(payment_t), 1, fp) > 0) {
		if(pay.userid == memberid)
			payment_display(&pay);
	}
	// close file	
	fclose(fp);
}