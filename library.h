
#ifndef _LIBRARY_H
#define _LIBRARY_H
#include"date.h"
#define USER_DB		"users.db"
#define BOOK_DB		"books.db"
#define BOOKCOPY_DB		"bookcopies.db"
#define ISSUERECORD_DB  "issuerecords.db"
#define PAYMENT_DB        "payment.db"


#define ROLE_OWNER 		"owner"
#define ROLE_LIBRARIAN 	"librarian"
#define ROLE_MEMBER 	"member"

#define STATUS_AVAIL	"available"
#define STATUS_ISSUED	"issued"

#define PAY_TYPE_FEES	"fees"
#define PAY_TYPE_FINE	"fine"

#define FINE_PER_DAY			5
#define BOOK_RETURN_DAYS		7
#define MEMBERSHIP_MONTH_DAYS	30

#define EMAIL_OWNER		"tsourabh3@gmail.com"


typedef struct user{
    int id;
    char name[30];
    char email[30];
    char phone[15];
    char password[15];
    char role[20];
}user_t;

typedef struct book{
    int id;
    char name[60];
    char author[60];
    char subject[60];
    float price;
    char isbn[16];
}book_t;

typedef struct bookcopy{
int id;
int bookid;
int rack;
char status[30];
}bookcopy_t;

typedef struct payment{
  int id;
  int userid;
  float amount;
  char type[30]; 
  date_t tx_time;
  date_t nextpayment_duedate;

}payment_t;

typedef struct issuerecord{
    int id;
    int copyid;
    int memberid;
    float fine_amount;
    date_t issue_date;
    date_t return_duedate;
    date_t return_date;
}issuerecord_t;

// owner area
void owner_area(user_t *u);
int menu_list();
void appoint_librarian();

//librarian area
void librarian_area(user_t *u);
int lab_menu_list();
void add_member();
void add_book();
void book_edit_by_id();
void bookcopy_add();
void bookcopy_checkavail_details();
void bookcopy_changestatus(int bookcopy_id, char status[]);
void bookcopy_issue();
void bookcopy_return();
void display_issued_bookcopies(int member_id);
void change_rack();
int is_paid_member(int memberid);
void fees_payment_add();
void fine_payment_add(int memberid, float fine_amount);

//Member area
void member_area(user_t *t);

//common functionalities
void user_accept(user_t *u);
void user_display(user_t *u);

void book_accept(book_t *b);
void book_display(book_t *b);

void sign_up();
void user_add(user_t *u);
int search_user_by_email(user_t *u,char email[]);
void book_find_by_name(char name[]);
void bookcopy_accept(bookcopy_t *b);
void bookcopy_display(bookcopy_t *c);

int get_next_user_id();
int get_next_book_id();
int get_next_bookcopy_id();
int get_next_issuerecord_id();
int get_next_paymentrecord_id();

void issuerecord_accept(issuerecord_t *rec);
void issuerecord_display(issuerecord_t *r);
void payment_accept(payment_t *p);
void payment_display(payment_t *p);
void payment_history(int memberid);

void edit_profile(user_t *u);
void edit_password(user_t *u);


#endif